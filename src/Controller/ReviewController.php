<?php

namespace App\Controller;

use App\Entity\Car;
use App\Repository\ReviewRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class ReviewController extends AbstractController
{
    public function __construct(
        private readonly ReviewRepository $reviewRepository
    ) {}

    /**
     * @param Car $car
     * @return JsonResponse
     */
    public function __invoke(Car $car): JsonResponse
    {
        $reviews = $this->reviewRepository->findLatestReviews($car->getId());
        return $this->json($reviews, 200, []);
    }
}
