<?php

namespace App\DataFixtures;

use App\Entity\Car;
use App\Entity\Review;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ReviewFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $reviews = [
            [
                'star' => 6,
                'text' => 'Very well',
                'car' => $this->getReference('car-BMW')
            ],
            [
                'star' => 8,
                'text' => 'Well designed',
                'car' => $this->getReference('car-BMW')
            ],
            [
                'star' => 9,
                'text' => 'Well designed',
                'car' => $this->getReference('car-Audi')
            ],
            [
                'star' => 6,
                'text' => 'Awesome!',
                'car' => $this->getReference('car-Audi')
            ],
            [
                'star' => 10,
                'text' => 'Nice!',
                'car' => $this->getReference('car-GMC')
            ],
            [
                'star' => 9,
                'text' => 'Wow!',
                'car' => $this->getReference('car-Chevrolet'),
            ],
            [
                'star' => 8,
                'text' => 'Excellent',
                'car' => $this->getReference('car-Chevrolet'),
            ],
            [
                'star' => 6,
                'text' => 'Not a fan!',
                'car' => $this->getReference('car-Alfa Romeo'),
            ],
            [
                'star' => 3,
                'text' => 'Not bad.',
                'car' => $this->getReference('car-Alfa Romeo'),
            ],
            [
                'star' => 7,
                'text' => 'Cool!',
                'car' => $this->getReference('car-Alfa Romeo'),
            ]
        ];

        foreach ($reviews as $review) {
            $reviewObject = new Review();
            $reviewObject->setStar($review['star']);
            $reviewObject->setText($review['text']);
            $reviewObject->setCar($review['car']);
            $manager->persist($reviewObject);
        }

        $manager->flush();
    }
}
