<?php

namespace App\DataFixtures;

use App\Entity\Car;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CarFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $cars = [
            [
                'brand' => 'BMW',
                'model' => 'z9',
                'color' => 'Red',
            ],
            [
                'brand' => 'Alfa Romeo',
                'model' => '164',
                'color' => 'Black',
            ],
            [
                'brand' => 'Audi',
                'model' => '100',
                'color' => 'White',
            ],
            [
                'brand' => 'GMC',
                'model' => '1500 Club Coupe',
                'color' => 'Yellow',
            ],
            [
                'brand' => 'Chevrolet',
                'model' => '1500 Extended Cab',
                'color' => 'Red',
            ]
        ];

        foreach ($cars as $car) {
            $carObject = new Car();
            $carObject->setBrand($car['brand']);
            $carObject->setModel($car['model']);
            $carObject->setColor($car['color']);
            $manager->persist($carObject);
            $this->setReference('car-' . $carObject->getBrand(), $carObject);
        }

        $manager->flush();
    }
}
