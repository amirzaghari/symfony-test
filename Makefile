# =================================
# Variables                       #
# =================================
SPRINT=1.0.0#...............Version
DATE=`date +%Y-%m-%d-%H.%M.%S`
define ANNOUNCE_BODY

                     _        ______           _                _
     /\             (_)      |___  /          | |              (_)
    /  \   _ __ ___  _ _ __     / / __ _  __ _| |__   __ _ _ __ _
   / /\ \ | '_ ` _ \| | '__|   / / / _` |/ _` | '_ \ / _` | '__| |
  / ____ \| | | | | | | |     / /_| (_| | (_| | | | | (_| | |  | |
 /_/    \_\_| |_| |_|_|_|    /_____\__,_|\__, |_| |_|\__,_|_|  |_|
                                          __/ |
                                         |___/

endef
export ANNOUNCE_BODY
#==================================
# Start Program                   #
#==================================
## ——————————————————————————— 🎵 🐳 How to use this CLI 🐳 🎵 ———————————————————————————
help: ## Outputs this help screen
	@echo "${CG}"
	@echo "$$ANNOUNCE_BODY"
	@echo "${NC}"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
## ——————————————————————————— 🚀  Project Commands 🚀  ——————————————————————————————————
install: ## This command will install the project for the first time
	docker compose build --no-cache
	docker exec -it symfony-test-php-1 php bin/console doctrine:database:create
	docker exec -it symfony-test-php-1 php bin/console doctrine:schema:create
	docker exec -it symfony-test-php-1 php bin/console doctrine:fixtures:load
	docker exec -it symfony-test-php-1 php bin/console --env=test doctrine:database:create
	docker exec -it symfony-test-php-1 php bin/console --env=test doctrine:schema:create
start: ## This command runs the project for development
	docker compose up --pull --wait
down: ## This command Stops the server
	docker compose down --remove-orphans
test: ## This command run the tests using phpunit
	docker exec -it symfony-test-php-1 php bin/console --env=test doctrine:fixtures:load
	docker exec -it symfony-test-php-1 php bin/phpunit
