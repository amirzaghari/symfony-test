<?php

namespace App\Tests\Repository;

use App\Entity\Car;
use App\Entity\Review;
use App\Repository\ReviewRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ReviewRepositoryTest extends KernelTestCase
{
    private EntityManager $entityManager;
    private ReviewRepository $reviewRepository;

    /**
     * @throws \Exception
     */
    protected function setUp(): void
    {
        $this->entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->reviewRepository = $this->entityManager->getRepository(Review::class);
    }

    public function testFindLatestReviews(): void
    {
        // Create a test car entity
        $car = new Car();
        $car->setColor("Red");
        $car->setBrand("BMW");
        $car->setModel("z9");
        $this->entityManager->persist($car);

        // Create some test reviews
        $review1 = new Review();
        $review1->setCar($car);
        $review1->setStar(7);
        $this->entityManager->persist($review1);

        $review2 = new Review();
        $review2->setCar($car);
        $review2->setStar(8);
        $this->entityManager->persist($review2);

        // Should not be included in the result
        $review3 = new Review();
        $review3->setCar($car);
        $review3->setStar(5);
        $this->entityManager->persist($review3);

        $this->entityManager->flush();

        // Call the repository method
        $reviews = $this->reviewRepository->findLatestReviews($car->getId());

        // Assertions
        $this->assertGreaterThanOrEqual(2, $reviews);
        $this->assertContains($review2, $reviews);
        $this->assertNotContains($review3, $reviews);
    }
}
